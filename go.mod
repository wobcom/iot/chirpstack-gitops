module gitlab.com/wobcom/iot/chirpstack-gitops

go 1.16

require (
	github.com/brocaar/chirpstack-api/go/v3 v3.9.7
	github.com/golang/protobuf v1.3.5
	github.com/google/go-cmp v0.5.2
	github.com/logrusorgru/aurora v2.0.3+incompatible
	github.com/rs/zerolog v1.19.0
	github.com/sosedoff/ansible-vault-go v0.0.0-20181205202858-ab5632c40bf5
	github.com/spf13/cobra v1.0.0
	github.com/stretchr/testify v1.6.1 // indirect
	golang.org/x/sys v0.0.0-20191010194322-b09406accb47 // indirect
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/grpc v1.28.0
	gopkg.in/yaml.v2 v2.2.3
)
