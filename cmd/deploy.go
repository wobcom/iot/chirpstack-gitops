// nolint: gochecknoglobals, gochecknoinits
package cmd

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/brocaar/chirpstack-api/go/v3/as/external/api"
	"github.com/logrusorgru/aurora"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/wobcom/iot/chirpstack-gitops/internal/config"
	"gitlab.com/wobcom/iot/chirpstack-gitops/internal/request"
	"gitlab.com/wobcom/iot/chirpstack-gitops/internal/worker"
	"google.golang.org/grpc"
)

var (
	numWorkers              = 10
	initialDeviceAllocation = 512
	requestTimeout          = 10 * time.Second
	requestLimit            = 500
	dc                      = config.DeployConfig{}
)

var deployCmd = &cobra.Command{
	Use:   "deploy",
	Short: "deploy",
	Run: func(cmd *cobra.Command, args []string) {
		RunDeploy()
	},
}

func init() {
	rootCmd.AddCommand(deployCmd)

	requiredFlags := []string{
		"vault-pass",
		"chirpstack-url",
		"chirpstack-token",
		"device-dir",
		"application-dir",
		"influx-url",
		"influx-username",
		"grafana-url",
		"grafana-token",
	}

	deployCmd.Flags().StringVarP(&dc.VaultPass, "vault-pass", "p", "", "vault password")

	deployCmd.Flags().StringVar(&dc.ChirpstackURL, "chirpstack-url", "", "chirpstack url")

	deployCmd.Flags().StringVar(&dc.ChirpstackToken, "chirpstack-token", "", "chirpstack api token")

	deployCmd.Flags().StringVarP(&dc.DeviceDir, "device-dir", "d", "", "device directory")

	deployCmd.Flags().StringVarP(&dc.ApplicationDir, "application-dir", "a", "", "application directory")

	deployCmd.Flags().StringVar(&dc.InfluxURL, "influx-url", "", "influx url")

	deployCmd.Flags().StringVar(&dc.InfluxUsername, "influx-username", "", "influx username")

	deployCmd.Flags().StringVar(&dc.InfluxPassword, "influx-password", "", "influx password")

	deployCmd.Flags().StringVar(&dc.GrafanaURL, "grafana-url", "", "grafana url")

	deployCmd.Flags().StringVar(&dc.GrafanaToken, "grafana-token", "", "grafana token")

	for _, flag := range requiredFlags {
		if err := deployCmd.MarkFlagRequired(flag); err != nil {
			log.Fatal().Msg(err.Error())
		}
	}
}

// nolint: gocognit,cyclop
func RunDeploy() {
	log.Info().Msgf("%v", dc)

	creds := &request.JwtCredentials{Token: dc.ChirpstackToken}

	inventory, err := config.ParseInventory(dc)
	if err != nil {
		log.Error().Err(err).Msg("parse inventory error.")
	}

	dialOpts := []grpc.DialOption{
		grpc.WithBlock(),
		grpc.WithInsecure(),
		grpc.WithPerRPCCredentials(creds),
	}

	ctxDial, cancelDial := context.WithTimeout(context.Background(), requestTimeout)
	defer cancelDial()

	conn, err := grpc.DialContext(ctxDial, dc.ChirpstackURL, dialOpts...)
	if err != nil {
		log.Error().Err(err).Msg("could not connect")

		return
	}

	appIds, err := worker.GetApplicationIds(conn, 1, requestLimit)
	if err != nil {
		log.Error().Err(err).Msg("Get applications failed.")
	}

	dpsID, err := worker.GetDeviceProfilesIds(conn, 1, requestLimit)
	if err != nil {
		log.Error().Err(err).Msg("Get device-profiles failed.")
	}

	done := make(chan bool)

	applicationCollector := make(map[string]*worker.ResultApplication, len(inventory.Applications))
	applicationTasks := make(chan worker.TaskApplication, numWorkers)
	applicationResults := make(chan *worker.ResultApplication, numWorkers)
	applicationSeen := make(map[string]bool, len(inventory.Applications))

	profileCollector := make(map[string]*worker.ResultDeviceProfile, len(inventory.DeviceProfiles))
	deviceProfileTasks := make(chan worker.TaskDeviceProfile, numWorkers)
	deviceProfileResults := make(chan *worker.ResultDeviceProfile, numWorkers)
	deviceProfileSeen := make(map[string]bool, len(inventory.DeviceProfiles))

	deviceCollector := make(map[string][]*worker.ResultDevice, initialDeviceAllocation)
	deviceTasks := make(chan worker.TaskDevice, numWorkers)
	deviceResults := make(chan *worker.ResultDevice, numWorkers)
	deviceSeen := make(map[string]bool, initialDeviceAllocation)

	for i := 1; i <= numWorkers; i++ {
		log.Info().Msgf("start worker %d...", i)

		dialOpts := []grpc.DialOption{
			grpc.WithBlock(),
			grpc.WithInsecure(),
			grpc.WithPerRPCCredentials(creds),
		}

		ctxDial, cancelDial := context.WithTimeout(context.Background(), requestTimeout)
		defer cancelDial()

		conn, err := grpc.DialContext(ctxDial, dc.ChirpstackURL, dialOpts...)
		if err != nil {
			log.Error().Err(err).Msg("could not connect")

			continue
		}

		defer conn.Close()

		dpClient := api.NewDeviceProfileServiceClient(conn)
		appClient := api.NewApplicationServiceClient(conn)
		devClient := api.NewDeviceServiceClient(conn)

		tlogger := log.Logger.With().Int("Worker", i).Logger()

		go worker.RunTaskDeviceProfiles(tlogger, deviceProfileTasks, deviceProfileResults, dpClient)
		go worker.RunTaskApplication(tlogger, applicationTasks, applicationResults, appClient)
		go worker.RunTaskDevices(tlogger, deviceTasks, deviceResults, devClient)
	}

	go worker.CollectDeviceProfiles(deviceProfileResults, profileCollector, done)

	var wgProfiles sync.WaitGroup

	for _, i := range inventory.DeviceProfiles {
		_, seen := deviceProfileSeen[i.Lora.Name]
		if !seen {
			deviceProfileSeen[i.Lora.Name] = true

			wgProfiles.Add(1)

			id, ok := dpsID[i.Lora.Name]
			if ok {
				i.Lora.Id = id
			}

			deviceProfileTasks <- worker.TaskDeviceProfile{
				DeviceProfile: i.Lora,
				Wg:            &wgProfiles,
			}
		} else {
			log.Warn().Msgf("device-profile %s duplicated", i.Lora.Name)

			continue
		}
	}

	close(deviceProfileTasks)
	wgProfiles.Wait()
	close(deviceProfileResults)

	<-done

	var wgApplication sync.WaitGroup

	go worker.CollectApplications(applicationResults, applicationCollector, done)

	for _, a := range inventory.Applications {
		_, seen := applicationSeen[a.Name]
		if seen {
			log.Warn().Msgf("application %s duplicated", a.Name)

			continue
		}

		applicationSeen[a.Name] = true

		wgApplication.Add(1)

		id, ok := appIds[a.Name]
		if ok {
			a.ID = id
		}

		applicationTasks <- worker.TaskApplication{
			Application: a,
			Wg:          &wgApplication,
		}
	}

	close(applicationTasks)
	wgApplication.Wait()
	close(applicationResults)

	go worker.CollectDevices(deviceResults, deviceCollector, done)

	<-done

	var wgDevices sync.WaitGroup

	for name := range applicationCollector {
		result := applicationCollector[name]
		if result.Status == worker.Failed {
			continue
		}

		for _, device := range result.Application.Devices {
			_, seen := deviceSeen[device.DevEUI]
			if seen {
				log.Warn().Msgf("device %s duplicated", device.DevEUI)

				continue
			}

			deviceSeen[device.DevEUI] = true

			profileResult, ok := profileCollector[device.DeviceProfileName]
			if !ok {
				continue
			}

			if profileResult.Status == worker.Failed {
				continue
			}

			wgDevices.Add(1)
			deviceTasks <- worker.TaskDevice{
				ApplicationID:   result.Application.ID,
				ApplicationName: result.Application.Name,
				Device:          device,
				Profile:         profileResult.DeviceProfile,
				Wg:              &wgDevices,
			}
		}
	}

	close(deviceTasks)
	wgDevices.Wait()
	close(deviceResults)

	<-done

	PrintResults(profileCollector, applicationCollector, deviceCollector)
}

func PrintResults(
	profileCollector map[string]*worker.ResultDeviceProfile,
	applicationCollector map[string]*worker.ResultApplication,
	deviceCollector map[string][]*worker.ResultDevice,
) {
	fmt.Print("\n\n======================================================\n\n")
	fmt.Print(aurora.BrightMagenta("Profiles\n").Bold().Underline())

	appColor := aurora.BrightMagenta("App>").Bold().Underline()
	integrationsColor := aurora.BrightMagenta("App>Integrations\n").Bold().Underline()
	deviceColor := aurora.BrightMagenta("App>Devices\n").Bold().Underline()

	for name, result := range profileCollector {
		fmt.Printf("%s\t%s\n", name, result.Status.Colored())
	}

	fmt.Print(aurora.BrightMagenta("\nApplications").Bold().Underline())

	for name, result := range applicationCollector {
		fmt.Printf("\n%s %s\t%s\n", appColor, name, result.Status.Colored())
		fmt.Print(integrationsColor)

		for iname, iStatus := range result.Integrations {
			fmt.Printf("%s\t%s\n", iname, iStatus.Colored())
		}

		fmt.Print(deviceColor)

		devResults, ok := deviceCollector[name]
		if !ok {
			fmt.Printf("No devices defined")

			continue
		}

		for _, devResult := range devResults {
			fmt.Printf("%s|%s\t%s\n", devResult.Device.DevEui, devResult.Device.Name, devResult.Status.Colored())
		}
	}
}
