package chirpstack

import (
	"fmt"
	"strings"

	"github.com/brocaar/chirpstack-api/go/v3/as/external/api"
	"github.com/google/go-cmp/cmp"
	"github.com/rs/zerolog/log"
)

// EqualProfile checks if profiles are equal on specific keys.
// nolint: cyclop
func EqualProfile(name string, a, b *api.DeviceProfile) bool {
	logger := log.With().Str("name", name).Logger()

	if !cmp.Equal(a.MacVersion, b.MacVersion) {
		logger.Debug().Str("field", "macVersion").Msg(cmp.Diff(a.MacVersion, b.MacVersion))

		return false
	}

	if !cmp.Equal(a.RegParamsRevision, b.RegParamsRevision) {
		logger.Debug().Str("field", "regParamsRevision").Msg(cmp.Diff(a.RegParamsRevision, b.RegParamsRevision))

		return false
	}

	if !cmp.Equal(a.MaxEirp, b.MaxEirp) {
		logger.Debug().Str("field", "MaxEirp").Msg(cmp.Diff(a.MaxEirp, b.MaxEirp))

		return false
	}

	if a.UplinkInterval != nil && b.UplinkInterval != nil {
		if !cmp.Equal(a.UplinkInterval, b.UplinkInterval) {
			logger.Debug().Str("field", "uplinkInterval").Msg(cmp.Diff(a.UplinkInterval, b.UplinkInterval))

			return false
		}
	}

	if !cmp.Equal(a.SupportsJoin, b.SupportsJoin) {
		logger.Debug().Str("field", "supportsJoin").Msg(cmp.Diff(a.SupportsJoin, b.SupportsJoin))

		return false
	}

	if !cmp.Equal(a.SupportsClassB, b.SupportsClassB) {
		logger.Debug().Str("field", "supportsClassB").Msg(cmp.Diff(a.SupportsClassB, b.SupportsClassB))

		return false
	}

	if !cmp.Equal(a.SupportsClassC, b.SupportsClassC) {
		logger.Debug().Str("field", "supportsClassC").Msg(cmp.Diff(a.SupportsClassC, b.SupportsClassC))

		return false
	}

	if !cmp.Equal(a.PayloadDecoderScript, b.PayloadDecoderScript) {
		logger.Debug().
			Str("field", "PayloadDecoderScript").
			Msg(cmp.Diff(a.PayloadDecoderScript, b.PayloadDecoderScript))

		return false
	}

	return true
}

// EqualDevice checks if devices are equal on specific keys.
func EqualDevice(a, b *api.Device) bool {
	logger := log.With().Str("dev", a.DevEui).Logger()

	if !cmp.Equal(a.DevEui, b.DevEui) {
		logger.Debug().Str("field", "DeviceProfileID").Msg(cmp.Diff(a.DevEui, b.DevEui))

		return false
	}

	if !cmp.Equal(a.DeviceProfileId, b.DeviceProfileId) {
		logger.Debug().Str("field", "DeviceProfileID").Msg(cmp.Diff(a.DeviceProfileId, b.DeviceProfileId))

		return false
	}

	if !cmp.Equal(a.Name, b.Name) {
		logger.Debug().Str("field", "Name").Msg(cmp.Diff(a.Name, b.Name))

		return false
	}

	if !cmp.Equal(a.Description, b.Description) {
		logger.Debug().Str("field", "Description").Msg(cmp.Diff(a.Description, b.Description))
		a.Description = b.Description

		return false
	}

	if !cmp.Equal(a.ApplicationId, b.ApplicationId) {
		logger.Debug().Str("field", "ApplicationID").Msg(cmp.Diff(fmt.Sprint(a.ApplicationId), b.ApplicationId))

		return false
	}

	if !cmp.Equal(a.Tags, b.Tags) {
		logger.Debug().Str("field", "Tags").Msg(cmp.Diff(a.Tags, b.Tags))

		return false
	}

	if !cmp.Equal(a.Variables, b.Variables) {
		logger.Debug().Str("field", "Tags").Msg(cmp.Diff(a.Tags, b.Tags))

		return false
	}

	return true
}

// EqualKeys compares keys got from API and from config.
func EqualKeys(apiKeys *api.DeviceKeys, configKey string, loraVersion string) bool {
	switch {
	case strings.HasPrefix(loraVersion, "1.0"):
		return cmp.Equal(apiKeys.NwkKey, configKey)
	case strings.HasPrefix(loraVersion, "1.1"):
		return cmp.Equal(apiKeys.AppKey, configKey)
	default:
		return false
	}
}

func NewDeviceKey(devEui string, configKey string, loraVersion string) *api.DeviceKeys {
	keys := api.DeviceKeys{}
	keys.DevEui = devEui

	switch {
	case strings.HasPrefix(loraVersion, "1.0"):
		keys.NwkKey = configKey
		keys.AppKey = "00000000000000000000000000000000"
		keys.GenAppKey = "00000000000000000000000000000000"
	case strings.HasPrefix(loraVersion, "1.1"):
		keys.AppKey = configKey
		keys.NwkKey = "00000000000000000000000000000000"
		keys.GenAppKey = "00000000000000000000000000000000"
	}

	return &keys
}

func EqualApplications(a, b *api.Application) bool {
	logger := log.With().Str("application", a.Name).Logger()
	equal := true

	if !cmp.Equal(a.Name, b.Name) {
		logger.Debug().Str("field", "Name").Msg(cmp.Diff(a.Name, b.Name))

		equal = false
	}

	if !cmp.Equal(a.Description, b.Description) {
		logger.Debug().Str("field", "Description").Msg(cmp.Diff(a.Description, b.Description))

		equal = false
	}

	if !cmp.Equal(a.OrganizationId, b.OrganizationId) {
		logger.Debug().Str("field", "OrganizationID").Msg(cmp.Diff(a.OrganizationId, b.OrganizationId))

		equal = false
	}

	if !cmp.Equal(a.ServiceProfileId, b.ServiceProfileId) {
		logger.Debug().Str("field", "ServiceProfileID").Msg(cmp.Diff(a.ServiceProfileId, b.ServiceProfileId))

		equal = false
	}

	return equal
}

func EqualIntegrationInfluxDB(a, b *api.InfluxDBIntegration) bool {
	logger := log.With().Int64("applicationID", a.ApplicationId).Logger()

	equal := true

	if !cmp.Equal(a.ApplicationId, b.ApplicationId) {
		logger.Debug().Str("field", "ApplicationID").Msg(
			cmp.Diff(a.ApplicationId, b.ApplicationId),
		)

		equal = false
	}

	if !cmp.Equal(a.Db, b.Db) {
		logger.Debug().Str("field", "DB").Msg(
			cmp.Diff(a.Db, b.Db),
		)

		equal = false
	}

	if !cmp.Equal(a.Endpoint, b.Endpoint) {
		logger.Debug().Str("field", "Endpoint").Msg(
			cmp.Diff(a.Endpoint, b.Endpoint),
		)

		equal = false
	}

	if !cmp.Equal(a.Password, b.Password) {
		logger.Debug().Str("field", "Password").Msg(
			cmp.Diff(a.Password, b.Password),
		)

		equal = false
	}

	if !cmp.Equal(a.Password, b.Password) {
		equal = false
	}

	if !cmp.Equal(a.Username, b.Username) {
		logger.Debug().Str("field", "Username").Msg(
			cmp.Diff(a.Username, b.Username),
		)

		equal = false
	}

	if !cmp.Equal(a.Precision, b.Precision) {
		logger.Debug().Str("field", "Precision").Msg(
			cmp.Diff(a.Precision, b.Precision),
		)

		equal = false
	}

	return equal
}
