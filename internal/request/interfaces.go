package request

import "io"

//go:generate mockery --name Requester

type Requester interface {
	Request(method, endpoint string, body io.Reader) (int, []byte, error)
}
