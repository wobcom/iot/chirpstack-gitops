// nolint: gochecknoglobals
package request

import (
	"context"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/rs/zerolog"
)

var timeout = 30 * time.Second

type APICall struct {
	BaseURL string
	Headers map[string]string
	Logger  zerolog.Logger
}

// NewAPICall creates a new APICall object.
func NewAPICall(baseURL string, headers map[string]string, logger zerolog.Logger) *APICall {
	return &APICall{baseURL, headers, logger}
}

// Request is a HTTP request abstraction to do the API requests.
func (a *APICall) Request(method, endpoint string, body io.Reader) (int, []byte, error) {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	url := a.BaseURL + "/" + endpoint

	logger := a.Logger.With().Str("method", method).Str("url", url).Logger()

	req, err := http.NewRequest(method, url, body)
	if err != nil {
		logger.Error().Msg(err.Error())

		return 0, []byte{}, err
	}

	for k, v := range a.Headers {
		req.Header.Set(k, v)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")

	c := &http.Client{}

	logger.Debug().Msg("doing the request")

	resp, err := c.Do(req.WithContext(ctx))
	if err != nil {
		logger.Error().Msg(err.Error())

		return 0, []byte{}, err
	}

	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logger.Error().Int("status_code", resp.StatusCode).Msg(err.Error())

		return 0, []byte{}, err
	}

	logger.Debug().Int("status_code", resp.StatusCode).Msg("got response")

	return resp.StatusCode, data, nil
}
