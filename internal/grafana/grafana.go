// nolint: gomnd, goerr113
package grafana

import (
	"bytes"
	"encoding/json"
	"fmt"

	"gitlab.com/wobcom/iot/chirpstack-gitops/internal/request"
)

type DataSource struct {
	Name      string   `json:"name"`
	Type      string   `json:"type"`
	URL       string   `json:"url"`
	Access    string   `json:"access"`
	BasicAuth bool     `json:"basicAuth"`
	Database  string   `json:"database"`
	User      string   `json:"user"`
	Password  string   `json:"password"`
	JSONData  JSONData `json:"jsonData"`
}

type JSONData struct {
	HTTPMode string `json:"httpMode"`
}

func DataSourceExists(ac request.Requester, name string) (bool, error) {
	status, _, err := ac.Request("GET", "api/datasources/name/"+name, nil)
	if err != nil {
		return false, err
	}

	switch {
	case status == 200:
		return true, nil
	case status == 404:
		return false, nil
	default:
		return false, fmt.Errorf("go a strange status code: %d", status)
	}
}

func CreateDataSource(ac request.Requester, source DataSource) error {
	s, err := json.Marshal(source)
	if err != nil {
		return err
	}

	status, data, err := ac.Request("POST", "api/datasources", bytes.NewBuffer(s))
	if err != nil {
		return err
	}

	if status != 200 {
		return fmt.Errorf("something went wrong %d: %s", status, string(data))
	}

	return nil
}
