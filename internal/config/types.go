package config

import (
	"github.com/brocaar/chirpstack-api/go/v3/as/external/api"
	"gitlab.com/wobcom/iot/chirpstack-gitops/internal/grafana"
)

type Device struct {
	DevEUI            string            `yaml:"dev_eui"`
	Name              string            `yaml:"name"`
	Description       string            `yaml:"description"`
	DeviceProfileName string            `yaml:"device_profile_name"`
	Key               string            `yaml:"key"`
	Tags              map[string]string `json:"tags"`
}

type Payload struct {
	Hex  string `json:"hex"`
	Port int    `json:"port"`
}

type Application struct {
	ID               int64
	Name             string        `yaml:"name"`
	Description      string        `yaml:"description"`
	ServiceProfileID string        `yaml:"service_profile_id"`
	OrganizationID   int64         `yaml:"organization_id"`
	Integrations     *Integrations `yaml:"integrations"`
	Devices          []*Device     `yaml:"devices"`
}

type Integrations struct {
	Influx  InfluxIntegration  `yaml:"influx"`
	Grafana GrafanaIntegration `yaml:"grafana"`
}

type InfluxIntegration struct {
	Enabled             bool   `yaml:"enabled"`
	DB                  string `json:"db" yaml:"influx_db_name"`
	Endpoint            string `json:"endpoint"`
	Password            string `json:"password"`
	Precision           string `json:"precision"`
	RetentionPolicyName string `json:"retentionPolicyName" yaml:"retention_policy_name"`
	Username            string `json:"username"`
}

type GrafanaIntegration struct {
	Enabled    bool               `yaml:"enabled"`
	URL        string             `yaml:"url"`
	Token      string             `yaml:"token"`
	DataSource grafana.DataSource `yaml:"datasource"`
}

type DeviceProfile struct {
	Schema        string
	ConfigPayload []*Payload
	Lora          *api.DeviceProfile
}

type Inventory struct {
	Applications   map[string]*Application
	DeviceProfiles map[string]*DeviceProfile
}

type DeployConfig struct {
	ApplicationDir  string
	ChirpstackToken string
	ChirpstackURL   string
	DeviceDir       string
	InfluxURL       string
	InfluxUsername  string
	InfluxPassword  string
	JSONOutput      bool
	VaultPass       string
	GrafanaToken    string
	GrafanaURL      string
}
