package config

import (
	"bytes"
	"encoding/json"
	"io/fs"
	"io/ioutil"
	"os"
	"path"
	"strconv"
	"strings"

	"github.com/brocaar/chirpstack-api/go/v3/as/external/api"
	"github.com/golang/protobuf/jsonpb"
	"github.com/rs/zerolog/log"
	vault "github.com/sosedoff/ansible-vault-go"
	goYaml "gopkg.in/yaml.v2"
)

func ParseInventory(dc DeployConfig) (*Inventory, error) {
	// Step dir
	inv := Inventory{}

	var err error

	inv.Applications, err = ParseApplications(dc)
	if err != nil {
		return nil, err
	}

	inv.DeviceProfiles, err = ParseDeviceProfiles(dc.DeviceDir)
	if err != nil {
		return nil, err
	}

	return &inv, err
}

func ParseApplications(dc DeployConfig) (map[string]*Application, error) {
	var m map[string]*Application

	ds, err := os.ReadDir(dc.ApplicationDir)
	if err != nil {
		return nil, err
	}

	m = make(map[string]*Application, 512)

	for _, i := range ds {
		app, err := ParseApplication(dc, i)
		if err != nil {
			return nil, err
		}

		m[app.Name] = app
	}

	return m, nil
}

// nolint: cyclop
func ParseApplication(dc DeployConfig, i fs.DirEntry) (*Application, error) {
	logger := log.With().Str("file", i.Name()).Logger()
	logger.Info().Msg("handle")

	b, err := os.ReadFile(path.Join(dc.ApplicationDir, i.Name()))
	if err != nil {
		log.Fatal().Msg(err.Error())

		return nil, err
	}

	app := Application{}

	if err := goYaml.Unmarshal(b, &app); err != nil {
		log.Error().Msg("decoding error")

		return nil, err
	}

	// set global defaults if missing

	if app.Integrations.Influx.Enabled {
		err := ParseInflux(&app, &dc)
		if err != nil {
			return nil, err
		}
	}

	if app.Integrations.Grafana.Enabled {
		err := ParseGrafana(&app, &dc)
		if err != nil {
			return nil, err
		}
	}

	for j, i := range app.Devices {
		// log.Info().Msgf("Device-profile-name: %s", i.DeviceProfileName)
		app.Devices[j].DevEUI = strings.ToLower(i.DevEUI)
		if app.Devices[j].Tags == nil {
			app.Devices[j].Tags = make(map[string]string)
		}

		if _, ok := app.Devices[j].Tags["productive"]; !ok {
			app.Devices[j].Tags["productive"] = strconv.FormatBool(false)
		}

		if i.Key != "" {
			sec, err := vault.Decrypt(i.Key, dc.VaultPass)
			if err != nil {
				return nil, err
			}

			// Store the lowercased version of the key.
			app.Devices[j].Key = strings.ToLower(sec)
		}
	}

	return &app, nil
}

func ParseDeviceProfiles(profilesDirs string) (map[string]*DeviceProfile, error) {
	var m map[string]*DeviceProfile

	ds, err := os.ReadDir(profilesDirs)
	if err != nil {
		return nil, err
	}

	m = make(map[string]*DeviceProfile, 512)

	for _, d := range ds {
		if !d.IsDir() {
			continue
		}

		dp, err := ParseDeviceProfile(profilesDirs, d.Name())
		if err != nil {
			return nil, err
		}

		m[d.Name()] = dp
	}

	return m, nil
}

func ParseDeviceProfile(profilesDirs, profileName string) (*DeviceProfile, error) {
	logger := log.With().Str("profileName", profileName).Logger()
	logger.Info().Msg("handle")

	b, err := ioutil.ReadFile(path.Join(profilesDirs, profileName, "config.json"))
	if err != nil {
		log.Fatal().Msg(err.Error())

		return nil, err
	}

	var objmap map[string]json.RawMessage
	if err := json.Unmarshal(b, &objmap); err != nil {
		return nil, err
	}

	devProfile := DeviceProfile{}

	if err := json.Unmarshal(objmap["config_payload"], &devProfile.ConfigPayload); err != nil {
		return nil, err
	}

	unmarshaler := &jsonpb.Unmarshaler{
		AllowUnknownFields: true, // we don't want to fail on unknown fields
	}

	dpObj, err := objmap["lora"].MarshalJSON()
	if err != nil {
		return nil, err
	}

	dp := &api.DeviceProfile{PayloadCodec: "CUSTOM_JS"}
	if err := unmarshaler.Unmarshal(bytes.NewReader(dpObj), dp); err != nil {
		return nil, err
	}

	devProfile.Lora = dp
	// Read decoder.js.
	d, err := ioutil.ReadFile(path.Join(profilesDirs, profileName, "decoder.js"))
	if err != nil {
		return nil, err
	}

	devProfile.Lora.PayloadDecoderScript = strings.TrimSuffix(string(d), "\n")

	logger.Info().Msgf("%s", devProfile.Lora.Name)

	return &devProfile, nil
}

func ParseInflux(app *Application, dc *DeployConfig) error {
	if app.Integrations.Influx.Endpoint == "" {
		app.Integrations.Influx.Endpoint = dc.InfluxURL
	}

	if app.Integrations.Influx.DB == "" {
		app.Integrations.Influx.DB = app.Name
	}

	if app.Integrations.Influx.Username == "" {
		app.Integrations.Influx.Username = dc.InfluxUsername
	}

	if app.Integrations.Influx.Password != "" {
		sec, err := vault.Decrypt(app.Integrations.Influx.Password, dc.VaultPass)
		if err != nil {
			return err
		}

		app.Integrations.Influx.Password = sec
	} else {
		app.Integrations.Influx.Password = dc.InfluxPassword
	}

	if app.Integrations.Influx.Precision == "" {
		app.Integrations.Influx.Precision = "NS"
	}

	return nil
}

// nolint: cyclop
func ParseGrafana(app *Application, dc *DeployConfig) error {
	if app.Integrations.Grafana.URL == "" {
		app.Integrations.Grafana.URL = dc.GrafanaURL
	}

	if app.Integrations.Grafana.Token != "" {
		sec, err := vault.Decrypt(app.Integrations.Grafana.Token, dc.VaultPass)
		if err != nil {
			return err
		}

		app.Integrations.Grafana.Token = sec
	} else {
		app.Integrations.Grafana.Token = dc.GrafanaToken
	}

	if app.Integrations.Grafana.DataSource.Type == "" {
		app.Integrations.Grafana.DataSource.Type = "influx"
	}

	if app.Integrations.Grafana.DataSource.Name == "" {
		app.Integrations.Grafana.DataSource.Name = app.Name
	}

	if app.Integrations.Grafana.DataSource.Access == "" {
		app.Integrations.Grafana.DataSource.Access = "proxy"
	}

	if app.Integrations.Grafana.DataSource.User == "" {
		app.Integrations.Grafana.DataSource.User = dc.InfluxUsername
	}

	if app.Integrations.Grafana.DataSource.Database == "" {
		app.Integrations.Grafana.DataSource.Database = app.Name
	}

	if app.Integrations.Grafana.DataSource.URL == "" {
		app.Integrations.Grafana.DataSource.URL = dc.InfluxURL
	}

	if app.Integrations.Grafana.DataSource.JSONData.HTTPMode == "" {
		app.Integrations.Grafana.DataSource.JSONData.HTTPMode = "GET"
	}

	return nil
}
