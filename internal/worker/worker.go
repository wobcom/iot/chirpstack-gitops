// nolint: gochecknoglobals
package worker

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/brocaar/chirpstack-api/go/v3/as/external/api"
	"github.com/logrusorgru/aurora"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/wobcom/iot/chirpstack-gitops/internal/chirpstack"
	"gitlab.com/wobcom/iot/chirpstack-gitops/internal/config"
	"gitlab.com/wobcom/iot/chirpstack-gitops/internal/grafana"
	"gitlab.com/wobcom/iot/chirpstack-gitops/internal/influx"
	"gitlab.com/wobcom/iot/chirpstack-gitops/internal/request"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var requestTimeout = 10 * time.Second

type TaskStatus int8

const (
	Failed TaskStatus = iota
	Created
	Updated
	OK
)

func (d TaskStatus) String() string {
	return [...]string{"Failed", "Created", "Updated", "OK"}[d]
}

func (d TaskStatus) Colored() string {
	return [...]string{
		aurora.Red("Failed").String(),
		aurora.Blue("Created").String(),
		aurora.Yellow("Updated").String(),
		aurora.Green("OK").String(),
	}[d]
}

type TaskDeviceProfile struct {
	DeviceProfile *api.DeviceProfile
	Wg            *sync.WaitGroup
}

type ResultDeviceProfile struct {
	Status        TaskStatus
	DeviceProfile *api.DeviceProfile
}

type TaskApplication struct {
	Application *config.Application
	Wg          *sync.WaitGroup
}

type ResultApplication struct {
	Status       TaskStatus
	Application  *config.Application
	Integrations map[string]TaskStatus
}

type TaskDevice struct {
	ApplicationID   int64
	ApplicationName string
	Device          *config.Device
	Profile         *api.DeviceProfile
	Wg              *sync.WaitGroup
}

type ResultDevice struct {
	Status          TaskStatus
	ApplicationName string
	Device          *api.Device
	SubTasks        map[string]TaskStatus
}

func RunTaskDeviceProfiles(
	tlogger zerolog.Logger, tasks <-chan TaskDeviceProfile,
	results chan<- *ResultDeviceProfile, dpClient api.DeviceProfileServiceClient) {
	logger := tlogger.With().Str("Task", "device-profile").Logger()
	logger.Info().Msg("running")

	for task := range tasks {
		result := HandleTaskDeviceProfiles(logger, task, dpClient)
		results <- result
	}
}

func HandleTaskDeviceProfiles(
	logger zerolog.Logger, task TaskDeviceProfile,
	dpClient api.DeviceProfileServiceClient) *ResultDeviceProfile {
	logger.Info().Msgf("Processing device-profile %s", task.DeviceProfile.Name)

	result := ResultDeviceProfile{
		DeviceProfile: task.DeviceProfile,
	}

	ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
	defer cancel()

	defer task.Wg.Done()

	if task.DeviceProfile.Id == "" {
		resp, err := dpClient.Create(ctx, &api.CreateDeviceProfileRequest{DeviceProfile: task.DeviceProfile})
		if err != nil {
			logger.Error().Err(err).Msg("create error")

			result.Status = Failed

			return &result
		}

		task.DeviceProfile.Id = resp.Id
		result.Status = Created

		logger.Info().Msgf("Device-profile %s created", task.DeviceProfile.Name)

		return &result
	}

	resp, err := dpClient.Get(ctx, &api.GetDeviceProfileRequest{Id: task.DeviceProfile.Id})
	if err != nil {
		logger.Error().Err(err).Msg("update error")

		result.Status = Failed

		return &result
	}

	if !chirpstack.EqualProfile(task.DeviceProfile.Name, resp.DeviceProfile, task.DeviceProfile) {
		_, err := dpClient.Update(ctx, &api.UpdateDeviceProfileRequest{DeviceProfile: task.DeviceProfile})
		if err != nil {
			logger.Error().Err(err).Msg("update error")

			result.Status = Failed

			return &result
		}

		result.Status = Updated

		logger.Info().Msgf("Device-profile %s updated", task.DeviceProfile.Name)

		return &result
	}

	result.Status = OK

	logger.Info().Msgf("Device-profile %s not changed", task.DeviceProfile.Name)

	return &result
}

func CollectDeviceProfiles(
	results <-chan *ResultDeviceProfile, collector map[string]*ResultDeviceProfile, done chan<- bool) {
	for result := range results {
		collector[result.DeviceProfile.Name] = result
	}
	done <- true
}

func RunTaskApplication(
	tlogger zerolog.Logger, tasks <-chan TaskApplication,
	results chan<- *ResultApplication, appClient api.ApplicationServiceClient) {
	logger := tlogger.With().Str("task", "application").Logger()
	logger.Info().Msg("running")

	for task := range tasks {
		result := HandleTaskApplication(logger, task, appClient)
		results <- result
	}

	logger.Info().Msg("done")
}

func HandleTaskApplication(
	logger zerolog.Logger, task TaskApplication, appClient api.ApplicationServiceClient) *ResultApplication {
	logger.Info().Msgf("Processing %s", task.Application.Name)

	ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
	defer cancel()

	defer task.Wg.Done()

	result := DoApplication(ctx, logger, task, appClient)

	if task.Application.Integrations.Influx.Enabled {
		result.Integrations["influx"] = DoInflux(ctx, logger, task, result.Application, appClient)
	}

	if task.Application.Integrations.Grafana.Enabled {
		result.Integrations["grafana"] = DoGrafana(logger, task, result.Application)
	}

	return result
}

func DoApplication(
	ctx context.Context, logger zerolog.Logger, task TaskApplication,
	appClient api.ApplicationServiceClient) *ResultApplication {
	app := &api.Application{
		Name:             task.Application.Name,
		Description:      task.Application.Description,
		OrganizationId:   task.Application.OrganizationID,
		ServiceProfileId: task.Application.ServiceProfileID,
	}
	result := &ResultApplication{Application: task.Application, Integrations: make(map[string]TaskStatus, 2)}

	if task.Application.ID == 0 {
		logger.Info().Msgf("Creating application %s", task.Application.Name)

		resp, err := appClient.Create(ctx, &api.CreateApplicationRequest{Application: app})
		if err != nil {
			logger.Error().Err(err).Msg("application creation error")

			result.Status = Failed

			return result
		}

		task.Application.ID = resp.Id

		return result
	}

	resp, err := appClient.Get(ctx, &api.GetApplicationRequest{Id: task.Application.ID})
	if err != nil {
		logger.Error().Err(err).Msg("application get error")

		result.Status = Failed

		return result
	}

	app.Id = task.Application.ID

	equal := chirpstack.EqualApplications(resp.Application, app)
	if !equal {
		logger.Info().Msgf("updating application %d|%s", app.Id, app.Name)

		_, err := appClient.Update(ctx, &api.UpdateApplicationRequest{Application: app})
		if err != nil {
			logger.Error().Err(err).Msg("application update error")

			result.Status = Failed

			return result
		}

		result.Status = Updated

		return result
	}

	logger.Info().Msgf("Application %d|%s not changed", app.Id, app.Name)

	result.Status = OK

	return result
}

// nolint: cyclop
func DoInflux(
	ctx context.Context, logger zerolog.Logger, task TaskApplication,
	app *config.Application, appClient api.ApplicationServiceClient) TaskStatus {
	logger.Info().Msg("Processing integration influx")

	flux := request.NewAPICall(task.Application.Integrations.Influx.Endpoint, map[string]string{}, log.Logger)

	dbExists, err := influx.DBExists(flux, task.Application.Integrations.Influx.DB)
	if err != nil {
		logger.Error().Err(err).Msgf("Application %d|%s influx-db error", app.ID, app.Name)

		return Failed
	}

	if !dbExists {
		if err := influx.CreateDB(flux, task.Application.Integrations.Influx.DB); err != nil {
			logger.Error().Err(err).Msgf("Application %d|%s influx-db error", app.ID, app.Name)

			return Failed
		}

		logger.Info().Msgf("Application %d|%s influx-db created", app.ID, app.Name)
	}

	logger.Info().Msgf("Application %d|%s influx-db ok", app.ID, app.Name)

	createInflux := false

	resp, err := appClient.GetInfluxDBIntegration(ctx, &api.GetInfluxDBIntegrationRequest{ApplicationId: app.ID})
	if err != nil {
		st, ok := status.FromError(err)
		if !ok || st.Code() != codes.NotFound {
			log.Error().Err(err).Msgf("get influx-integration error")

			return Failed
		}

		createInflux = true
	}

	integration := &api.InfluxDBIntegration{
		ApplicationId: task.Application.ID,
		Endpoint:      task.Application.Integrations.Influx.Endpoint,
		Db:            task.Application.Integrations.Influx.DB,
		Username:      task.Application.Integrations.Influx.Username,
		Password:      task.Application.Integrations.Influx.Password,
		Precision:     api.InfluxDBPrecision_NS,
	}

	if createInflux {
		_, err := appClient.CreateInfluxDBIntegration(ctx, &api.CreateInfluxDBIntegrationRequest{Integration: integration})
		if err != nil {
			logger.Error().Err(err).Msgf("Application %d|%s influx-intregration error", app.ID, app.Name)

			return Failed
		}

		logger.Info().Msgf("Application %d|%s influx-intregration created", app.ID, app.Name)

		return Created
	}

	equal := chirpstack.EqualIntegrationInfluxDB(resp.Integration, integration)
	if !equal {
		_, err := appClient.UpdateInfluxDBIntegration(ctx, &api.UpdateInfluxDBIntegrationRequest{Integration: integration})
		if err != nil {
			logger.Error().Err(err).Msgf("Application %d|%s influx-intregration error", app.ID, app.Name)

			return Failed
		}

		logger.Info().Msgf("Application %d|%s influx-intregration updated", app.ID, app.Name)

		return Updated
	}

	logger.Info().Msgf("Application %d|%s influx-intregration not changed", app.ID, app.Name)

	return OK
}

func DoGrafana(logger zerolog.Logger, task TaskApplication,
	app *config.Application) TaskStatus {
	logger.Info().Msg("Processing integration grafana")

	ds := task.Application.Integrations.Grafana.DataSource
	graf := request.NewAPICall(
		task.Application.Integrations.Grafana.URL,
		map[string]string{"Authorization": fmt.Sprintf("Bearer %s", task.Application.Integrations.Grafana.Token)},
		log.Logger,
	)

	exists, err := grafana.DataSourceExists(graf, ds.Name)
	if err != nil {
		logger.Error().Err(err).Msgf("Application %d|%s grafana-intregration error", app.ID, app.Name)

		return Failed
	}

	if !exists {
		err := grafana.CreateDataSource(graf, ds)
		if err != nil {
			logger.Error().Err(err).Msgf("Application %d|%s grafana-intregration error", app.ID, app.Name)

			return Failed
		}

		logger.Info().Msgf("Application %d|%s grafana-intregration created", app.ID, app.Name)

		return Created
	}

	logger.Info().Msgf("Application %d|%s grafana-intregration not changed", app.ID, app.Name)

	return OK
}

func CollectApplications(results <-chan *ResultApplication, collector map[string]*ResultApplication, done chan<- bool) {
	for result := range results {
		collector[result.Application.Name] = result
	}
	done <- true
}

func RunTaskDevices(
	tlogger zerolog.Logger, tasks <-chan TaskDevice, results chan<- *ResultDevice, devClient api.DeviceServiceClient) {
	logger := tlogger.With().Str("task", "device").Logger()

	for task := range tasks {
		result := HandleTaskDevices(logger, task, devClient)
		results <- result
	}
}

func CollectDevices(results <-chan *ResultDevice, collector map[string][]*ResultDevice, done chan<- bool) {
	for result := range results {
		devs, ok := collector[result.ApplicationName]
		if !ok {
			collector[result.ApplicationName] = []*ResultDevice{result}
		} else {
			collector[result.ApplicationName] = append(devs, result)
		}
	}
	done <- true
}

func HandleTaskDevices(logger zerolog.Logger, task TaskDevice, devClient api.DeviceServiceClient) *ResultDevice {
	// logger.Info().Msgf("Processing device %s", task.Device.DevEUI)
	ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
	defer cancel()
	defer task.Wg.Done()

	result := DoDevice(ctx, logger, task, devClient)
	// Keys
	if task.Device.Key != "" {
		result.SubTasks["keys"] = DoDeviceKeys(ctx, logger, task, devClient, result.Device)
	}

	return result
}

func DoDevice(
	ctx context.Context, logger zerolog.Logger,
	task TaskDevice, devClient api.DeviceServiceClient) *ResultDevice {
	dev := &api.Device{
		Name:            task.Device.Name,
		DevEui:          task.Device.DevEUI,
		ApplicationId:   task.ApplicationID,
		Description:     task.Device.Description,
		DeviceProfileId: task.Profile.Id,
		Tags:            task.Device.Tags,
		IsDisabled:      false,
	}
	create := false
	result := ResultDevice{ApplicationName: task.ApplicationName, Device: dev, SubTasks: make(map[string]TaskStatus, 2)}

	resp, err := devClient.Get(ctx, &api.GetDeviceRequest{DevEui: task.Device.DevEUI})
	if err != nil {
		st, ok := status.FromError(err)
		if !ok || st.Code() != codes.NotFound {
			logger.Error().Err(err).Msgf("device %s|%s get error", task.Device.DevEUI, task.Device.Name)

			result.Status = Failed

			return &result
		}

		create = true
	}

	if create {
		_, err := devClient.Create(ctx, &api.CreateDeviceRequest{Device: dev})
		if err != nil {
			logger.Error().Err(err).Msg("device %s|%s create error")

			result.Status = Failed

			return &result
		}

		result.Status = Created

		logger.Info().Msgf("Device %s|%s created", task.Device.DevEUI, task.Device.Name)

		return &result
	}

	equal := chirpstack.EqualDevice(resp.Device, dev)
	if !equal {
		_, err := devClient.Update(ctx, &api.UpdateDeviceRequest{Device: dev})
		if err != nil {
			logger.Error().Err(err).Msg("device %s|%s update error")

			result.Status = Failed

			return &result
		}

		logger.Info().Msgf("device %s|%s updated", task.Device.DevEUI, task.Device.Name)

		result.Status = Updated

		return &result
	}

	logger.Info().Msgf("Device %s|%s not changed", task.Device.DevEUI, task.Device.Name)

	result.Status = OK

	return &result
}

func DoDeviceKeys(
	ctx context.Context, logger zerolog.Logger, task TaskDevice,
	devClient api.DeviceServiceClient, device *api.Device) TaskStatus {
	create := false

	resp, err := devClient.GetKeys(ctx, &api.GetDeviceKeysRequest{DevEui: device.DevEui})
	if err != nil {
		st, ok := status.FromError(err)
		if !ok || st.Code() != codes.NotFound {
			log.Error().Err(err).Msgf("get device-keys error")

			return Failed
		}

		create = true
	}

	if create {
		newKeys := chirpstack.NewDeviceKey(task.Device.DevEUI, task.Device.Key, task.Profile.MacVersion)

		_, err := devClient.CreateKeys(ctx, &api.CreateDeviceKeysRequest{DeviceKeys: newKeys})
		if err != nil {
			log.Error().Err(err).Msgf("get device-keys error")

			return Failed
		}

		return Created
	}

	equal := chirpstack.EqualKeys(resp.DeviceKeys, task.Device.Key, task.Profile.MacVersion)
	if !equal {
		newKeys := chirpstack.NewDeviceKey(task.Device.DevEUI, task.Device.Key, task.Profile.MacVersion)

		_, err := devClient.UpdateKeys(ctx, &api.UpdateDeviceKeysRequest{DeviceKeys: newKeys})
		if err != nil {
			log.Error().Err(err).Msgf("get device-keys error")

			return Failed
		}

		logger.Info().Msgf("device %s|%s keys updated", task.Device.DevEUI, task.Device.Name)

		return Updated
	}

	logger.Info().Msgf("Device %s|%s keys not changed", task.Device.DevEUI, task.Device.Name)

	return OK
}
