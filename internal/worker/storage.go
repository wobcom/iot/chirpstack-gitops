package worker

import (
	"context"
	"errors"
	"fmt"

	"github.com/brocaar/chirpstack-api/go/v3/as/external/api"
	"google.golang.org/grpc"
)

var errDuplicateEntry = errors.New("errDuplicateEntry")

func DuplicateEntryError(msg string) error {
	return fmt.Errorf("%w: %s", errDuplicateEntry, msg)
}

func GetDeviceProfilesIds(conn grpc.ClientConnInterface, organizationID int64, limit int) (map[string]string, error) {
	dpClient := api.NewDeviceProfileServiceClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
	defer cancel()

	resp, err := dpClient.List(ctx, &api.ListDeviceProfileRequest{OrganizationId: organizationID, Limit: int64(limit)})
	if err != nil {
		return nil, err
	}

	result, err := DeviceProfileMapNameID(resp.Result)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func DeviceProfileMapNameID(list []*api.DeviceProfileListItem) (map[string]string, error) {
	result := make(map[string]string, len(list))

	for _, item := range list {
		_, seen := result[item.Name]
		if !seen {
			result[item.Name] = item.Id
		} else {
			return nil, DuplicateEntryError(fmt.Sprintf("device-profile %s", item.Name))
		}
	}

	return result, nil
}

func GetApplicationIds(conn grpc.ClientConnInterface, organizationID int64, limit int) (map[string]int64, error) {
	appClient := api.NewApplicationServiceClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
	defer cancel()

	resp, err := appClient.List(ctx, &api.ListApplicationRequest{OrganizationId: organizationID, Limit: int64(limit)})
	if err != nil {
		return nil, err
	}

	result, err := ApplicationMapNameID(resp.Result)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func ApplicationMapNameID(list []*api.ApplicationListItem) (map[string]int64, error) {
	result := make(map[string]int64, len(list))

	for _, item := range list {
		_, seen := result[item.Name]
		if !seen {
			result[item.Name] = item.Id
		} else {
			return nil, DuplicateEntryError(fmt.Sprintf("application %s", item.Name))
		}
	}

	return result, nil
}
