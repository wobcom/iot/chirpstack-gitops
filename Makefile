.PHONY: test lint

test:
	go test -v ./...

lint:
	golangci-lint run --enable-all --disable=funlen,godox,exhaustivestruct,wrapcheck,paralleltest,forbidigo

CHIRPSTACK_API_URL = chirpstack-as-external:8080
CHIRPSTACK_API_TOKEN = "API_JWT"
VAULT_PASS = supersecurepassword
INFLUX_URL = http://ts-wobcom-influxdb:8086
INFLUX_USERNAME = influxUser
INFLUX_PASSWORD = notasecurepassword
GRAFANA_URL = "http://grafana"
GRAFANA_TOKEN = "GRAFANA_API_TOKEN"
deploy:
	go run main.go deploy \
	--chirpstack-url ${CHIRPSTACK_API_URL} \
	--chirpstack-token ${CHIRPSTACK_API_TOKEN} \
	--vault-pass ${VAULT_PASS} \
	-d inventory/device-profiles/ \
	-a inventory/applications/ \
	--influx-url ${INFLUX_URL} \
	--influx-username ${INFLUX_USERNAME} \
	--influx-password ${INFLUX_PASSWORD} \
	--grafana-url ${GRAFANA_URL} \
	--grafana-token ${GRAFANA_TOKEN} \

